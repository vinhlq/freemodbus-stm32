#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <assert.h>
#include <sys/queue.h>
#include "board.h"
#include "wrap_printf.h"

static char printf_buffer[128];
static UART_HandleTypeDef *printf_uart_handle = NULL;

int __wrap_siprintf ( char * s, const char * format, ... )
{
	int n;
	va_list ap;

	va_start(ap, format);
	n=vsprintf(s, format, ap);
	va_end(ap);

	return n;
}

int __wrap_sniprintf ( char * s, size_t n, const char * format, ... )
{
	int nr;
	va_list ap;

	va_start(ap, format);
	nr=vsnprintf(s, n, format, ap);
	va_end(ap);

	return nr;
}

int __wrap_vprintf(const char * format, va_list ap)
{
	int n;

	n=vsnprintf(printf_buffer, sizeof(printf_buffer), format, ap);
	if(printf_uart_handle)
	{
		HAL_UART_Transmit(printf_uart_handle, (uint8_t*)printf_buffer, n, HAL_MAX_DELAY);
	}
	return n;
}
int __wrap_printf (const char * format, ... )
{
	va_list ap;
	int n;

	va_start(ap, format);
	n = __wrap_vprintf(format, ap);
	va_end(ap);

	return n;
}

int __wrap_fprintf (FILE * stream, const char * format, ... )
{
	int n;
	va_list ap;

	va_start(ap, format);
	n=__wrap_vprintf(format, ap);
	va_end(ap);
	return n;
}

int __wrap_puts ( const char * str )
{
	while(*str)
	{
		if(printf_uart_handle)
		{
			HAL_UART_Transmit(printf_uart_handle, (uint8_t*)str, 1, HAL_MAX_DELAY);
		}
	}
	return 0;
}

void __wrap_exit(int status)
{

}

void __wrap_abort(void)
{

}

void __wrap___assert_func( 	const char *  	file,
							int  	line,
							const char *  	func,
							const char *  	failedexpr
	)
{
	__wrap_printf("\r\nAssert failed: (%s:%d) %s: %s\r\n\r\n", file, line, func, failedexpr);
	abort();
	/* NOTREACHED */
}

void wrap_printf_set_uart_handle(UART_HandleTypeDef *uart_handle)
{
	printf_uart_handle = uart_handle;
}

/*
 * EOF
 */
