#ifndef MODBUS_SERIALPORT_H_
#define MODBUS_SERIALPORT_H_

#ifndef assert
#define assert( x )
#endif



// typedef char    BOOL;
// 
// typedef unsigned char UCHAR;
// typedef char    CHAR;
// 
// typedef unsigned short USHORT;
// typedef short   SHORT;
// 
// typedef unsigned long ULONG;
// typedef long    LONG;

#define SERIAL_PORT_MAX_INSTANCE    (1)

struct mb_ser_config
{

};

struct _mb_port_ser
{
    //!< Port base type
    mb_port_cb_struct  *cb; //!<Port callbacks.
    void               *arg; //!<CB arg pointer.
};

void
mb_port_serial_start_uart(mb_port_ser_struct* inst);

void
mb_port_serial_stop_uart(mb_port_ser_struct* inst);

void mb_port_serial_init(const struct mb_ser_config **config);

mb_port_ser_struct* mb_port_get_instance(USHORT index);
#endif
