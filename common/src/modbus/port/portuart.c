/*
 * FreeModbus Libary: stm32 Port
 */
#ifdef NDEBUG
#line __LINE__ "portuart.c"
#undef NDEBUG
#endif
#include <assert.h>
#include <string.h>

#include "board.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "port.h"
#include "mb.h"
#include "mbport.h"

mb_port_ser_struct mb_port_ser_obj[SERIAL_PORT_MAX_INSTANCE];

#if defined(MB_LED_PORT) && defined(MB_LED_PIN)
#define LED_ON()	HAL_GPIO_WritePin(MB_LED_PORT, MB_LED_PIN, GPIO_PIN_SET)
#define LED_OFF()	HAL_GPIO_WritePin(MB_LED_PORT, MB_LED_PIN, GPIO_PIN_RESET)
#else
#define LED_ON()
#define LED_OFF()
#endif

mb_port_ser_struct* mb_port_get_serial_instance(USHORT index)
{
	if(index < SERIAL_PORT_MAX_INSTANCE)
	{
		return &(mb_port_ser_obj[index]);
	}
	return NULL;
}

void mb_port_ser_enable  (mb_port_ser_struct* inst, BOOL rx_enable, BOOL tx_enable)
{
	/* If xRXEnable enable serial receive interrupts. If xTxENable enable
	 * transmitter empty interrupts.
	 */

	if (rx_enable)
	{
		__HAL_UART_ENABLE_IT(&mb_uart_handle, UART_IT_RXNE);
#if defined(MB_RS485_RE_PORT) && defined(MB_RS485_RE_PIN)
		HAL_GPIO_WritePin(MB_RS485_RE_PORT, MB_RS485_RE_PIN, GPIO_PIN_RESET);
#endif
	}
	else
	{
		__HAL_UART_DISABLE_IT(&mb_uart_handle, UART_IT_RXNE);
#if defined(MB_RS485_RE_PORT) && defined(MB_RS485_RE_PIN)
		HAL_GPIO_WritePin(MB_RS485_RE_PORT, MB_RS485_RE_PIN, GPIO_PIN_SET);
#endif
	}

	if (tx_enable)
	{
		__HAL_UART_ENABLE_IT(&mb_uart_handle, UART_IT_TXE);
#if defined(MB_RS485_DE_PORT) && defined(MB_RS485_DE_PIN)
		HAL_GPIO_WritePin(MB_RS485_DE_PORT, MB_RS485_DE_PIN, GPIO_PIN_SET);
#endif
		LED_ON();
	}
	else
	{
		__HAL_UART_DISABLE_IT(&mb_uart_handle, UART_IT_TXE);
#if defined(MB_RS485_DE_PORT) && defined(MB_RS485_DE_PIN)
		HAL_GPIO_WritePin(MB_RS485_DE_PORT, MB_RS485_DE_PIN, GPIO_PIN_RESET);
#endif
		LED_OFF();
	}
}

BOOL mb_port_ser_init(	mb_port_ser_struct* inst,
						ULONG baud,
						UCHAR data_bits,
						mb_port_ser_parity_enum parity)
{
	board_mb_uart_init(9600,
			UART_WORDLENGTH_8B,
			UART_STOPBITS_1,
			UART_PARITY_NONE);

    mb_port_ser_enable(inst, FALSE, FALSE);
    return TRUE;
}

void mb_port_ser_close   (mb_port_ser_struct* inst)
{

}

BOOL mb_port_ser_put_byte(mb_port_ser_struct* inst, CHAR byte_va0l)
{
	/* Put a byte in the UARTs transmit buffer. This function is called
	 * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
	 * called. */
	return (HAL_UART_Transmit(&mb_uart_handle, (uint8_t *)&byte_va0l, 1, 10) == HAL_OK);
}

BOOL mb_port_ser_get_byte(mb_port_ser_struct* inst, CHAR * byte_buf)
{
	/* Return the byte in the UARTs receive buffer. This function is called
	 * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
	 */
	return (HAL_UART_Receive(&mb_uart_handle, (uint8_t *)byte_buf, 1, 10) == HAL_OK);
}

static void mb_port_echo(void)
{
	uint8_t c;

	while(HAL_TIMEOUT != HAL_UART_Receive(&mb_uart_handle, &c, 1, 0))
	{
		HAL_UART_Transmit(&mb_uart_handle, &c, 1, 10);
	}
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
	LED_ON();
}

/**
  * @brief This function handles USART global interrupt.
  */
void MB_UART_IRQHandler(void)
{
	mb_port_ser_struct *inst = (mb_port_ser_struct*) &mb_port_ser_obj[0];

//	HAL_GPIO_TogglePin(LED_PORT, LED_PIN);
	if (__HAL_UART_GET_FLAG(&mb_uart_handle, UART_FLAG_RXNE)
			&& __HAL_UART_GET_IT_SOURCE(&mb_uart_handle, UART_IT_RXNE))
	{
		inst->cb->byte_rcvd(inst->arg); //RX not empty
//		mb_port_echo();
	}

	if (__HAL_UART_GET_FLAG(&mb_uart_handle, UART_FLAG_TXE)
			&& __HAL_UART_GET_IT_SOURCE(&mb_uart_handle, UART_IT_TXE))
	{
		inst->cb->tx_empty(inst->arg); //TX empty
	}

	HAL_UART_IRQHandler(&mb_uart_handle);
}

/*
 * EOF
 */
