/*
 * FreeModbus Libary: stm32 Port
 */

/* ----------------------- Platform includes --------------------------------*/

#include "board.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "port.h"
#include "mb.h"
#include "mbport.h"

/* ----------------------- Timer ------------------------------------------*/

TIM_HandleTypeDef mb_timer_handle;

BOOL mb_port_ser_tmr_init   (mb_port_ser_struct* inst, USHORT timeout_50us)
{
	/* TIMx Peripheral clock enable */
	MB_TIMER_CLK_ENABLE();
	/*##-2- Configure the NVIC for TIMx ########################################*/
	/* Set the TIMx priority */
	HAL_NVIC_SetPriority(MB_TIMER_IRQn, 10, 0);

	/* Enable the TIMx global Interrupt */
	HAL_NVIC_EnableIRQ(MB_TIMER_IRQn);

	mb_timer_handle.Instance = MB_TIMER;
	/* Compute the prescaler value to have TIMx counter clock equal to 20000 Hz */
	mb_timer_handle.Init.Prescaler = (uint32_t)(SystemCoreClock / 20000) - 1;
	mb_timer_handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	mb_timer_handle.Init.Period = timeout_50us - 1;
	mb_timer_handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	mb_timer_handle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_Base_Init(&mb_timer_handle) != HAL_OK)
	{
		while(1);
	}

	mb_timer_handle.Instance->CNT = 0;
	mb_timer_handle.Instance->ARR = timeout_50us - 1;

	return TRUE;
}

void timer_set_period(USHORT timeout_50us)
{
	mb_timer_handle.Instance->CNT = 0;
	mb_timer_handle.Instance->ARR = timeout_50us - 1;
	HAL_TIM_Base_Start_IT(&mb_timer_handle);
}

void mb_port_ser_tmr_close  (mb_port_ser_struct* inst)
{
	/* Disable any pending timers. */
	HAL_TIM_Base_Stop_IT(&mb_timer_handle);
}

void mb_port_ser_tmr_enable (mb_port_ser_struct* inst)
{
	/* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
	/* RESET TIMER */
	mb_timer_handle.Instance->CNT = 0;
	HAL_TIM_Base_Start_IT(&mb_timer_handle);
}

void mb_port_ser_tmr_disable(mb_port_ser_struct* inst)
{
	/* Disable any pending timers. */
	HAL_TIM_Base_Stop_IT(&mb_timer_handle);
}

void mb_port_ser_tmr_delay(mb_port_ser_struct* inst, USHORT timeout_ms)
{
  (void)inst;
  (void)timeout_ms;
  /*Not supproted*/
#if MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS > 0
#   error "MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS > 0 is not supported!!!"
#endif
}

#if MB_MASTER > 0
INLINE void mb_port_ser_tmr_convert_delay_enable  (mb_port_ser_struct* inst)
{
	mb_rtu_set_cur_tmr_mode(inst, MB_TMODE_CONVERT_DELAY);
	timer_set_period(MB_MASTER_DELAY_MS_CONVERT * 1000 / 50);
}
INLINE void mb_port_ser_tmr_respond_timeout_enable(mb_port_ser_struct* inst)
{
	mb_rtu_set_cur_tmr_mode(inst, MB_TMODE_RESPOND_TIMEOUT);
	timer_set_period(MB_MASTER_TIMEOUT_MS_RESPOND * 1000 / 50);
}
#endif

/**
  * @brief This function handles TIM1 trigger and commutation interrupts and TIM11 global interrupt.
  */
void MB_TIMER_IRQHandler(void)
{
  mb_port_ser_struct *inst;

  HAL_TIM_IRQHandler(&mb_timer_handle);

  inst = mb_port_get_serial_instance(0);
  if(!inst)
  {
	  return;
  }
  inst->cb->tmr_expired(inst->arg);
}
