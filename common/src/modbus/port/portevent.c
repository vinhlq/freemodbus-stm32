/*
 * FreeModbus Libary: ATMega168 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portevent.c,v 1.2 2006/05/14 21:55:01 wolti Exp $
 */

/* ----------------------- Modbus includes ----------------------------------*/
#include "port.h"
#include "mb.h"
#include "mbport.h"

/* ----------------------- Platform includes --------------------------------*/

/* ----------------------- Event ----------------------------------------*/
/* The queue is to be created to hold a maximum of 10 uint64_t
variables. */
#define QUEUE_LENGTH    5
#define ITEM_SIZE       sizeof( mb_event_enum )

/* The variable used to hold the queue's data structure. */
static StaticQueue_t static_queue;

/* The array to use as the queue's storage area.  This must be at least
uxQueueLength * uxItemSize bytes. */
static uint8_t queue_storage[ QUEUE_LENGTH * ITEM_SIZE ];

static QueueHandle_t mb_queue_handle;

BOOL mb_port_ser_evt_init(mb_port_ser_struct* inst)
{
	mb_queue_handle = xQueueCreateStatic(
											QUEUE_LENGTH,
											ITEM_SIZE,
											queue_storage,
											&static_queue);
    return TRUE;
}

BOOL mb_port_ser_evt_post(mb_port_ser_struct* inst, mb_event_enum event)
{
	BaseType_t result;

	result = xQueueSendFromISR(mb_queue_handle, ( const void * )&event, NULL );
	if(result)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL mb_port_ser_evt_get (mb_port_ser_struct* inst, void* caller, mb_event_enum * event)
{
	BOOL event_happened = FALSE;

//	if(pdTRUE == xQueueReceive( mb_queue_handle, event, portTICK_RATE_MS * 50 ))
	if(pdTRUE == xQueueReceive( mb_queue_handle, event, portMAX_DELAY))
	{
		event_happened = TRUE;
	}
	return event_happened;
}
