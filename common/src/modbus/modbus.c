/***************************************************************************//**
 * @file
 * @brief Routines for the Poll Control Server plugin, which implement the
 *        server side of the Poll Control cluster. The Poll Control cluster
 *        provides a means to communicate with an end device with a sleep
 *        schedule.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include "mb.h"
#include "mbport.h"
#include "modbus.h"

static const struct mb_ser_config mb_port_ser_config1 =
{

};
static const struct mb_ser_config mb_port_ser_config2 =
{

};

static const struct mb_ser_config *mb_port_ser_config[] =
{
  &mb_port_ser_config1,
  &mb_port_ser_config2,
  NULL
};

static mb_trans_union transport_obj[SERIAL_PORT_MAX_INSTANCE];
static mb_inst_struct mb_inst_struct_obj[SERIAL_PORT_MAX_INSTANCE];

mb_inst_struct* mb_get_instance(USHORT index)
{
	if(index < SERIAL_PORT_MAX_INSTANCE)
	{
		return &(mb_inst_struct_obj[index]);
	}
	return NULL;
}

mb_err_enum modbus_transport_init(
		USHORT index,
		mb_mode_enum mode,
		BOOL is_master,
		UCHAR slv_addr,
		ULONG baud,
		mb_port_ser_parity_enum parity)
{
	mb_err_enum status;
	mb_inst_struct *inst;
	mb_port_ser_struct *ser_inst;

	ser_inst = mb_port_get_serial_instance(index);
	if (!ser_inst)
	{
		return MB_EINVAL;
	}
	inst = mb_get_instance(index);
	if (!inst)
	{
		return MB_EINVAL;
	}

	status = mb_init(inst,
			transport_obj,  // mb_trans_union *transport,
			mode, // mb_mode_enum mode,
			is_master,  // BOOL is_master,
			slv_addr, //UCHAR slv_addr,
			(mb_port_base_struct*) ser_inst, // mb_port_base_struct * port_obj,
			baud, // ULONG baud,
			parity // mb_port_ser_parity_enum parity);
			);
	return status;
}

mb_err_enum modbus_enable(USHORT index)
{
	mb_inst_struct *inst;

	inst = mb_get_instance(index);
	if (!inst)
	{
		return MB_EINVAL;
	}
	return mb_enable(inst);
}

mb_err_enum modbus_set_slv_id(
		USHORT index,
		UCHAR slv_id,
		BOOL is_running,
		UCHAR const *slv_idstr,
		USHORT slv_idstr_len)
{
	mb_inst_struct *inst;

	inst = mb_get_instance(index);
	if (!inst)
	{
		return MB_EINVAL;
	}

	return mb_set_slv_id(inst, slv_id, is_running, slv_idstr, slv_idstr_len);
}
