/***************************************************************************//**
 * @file
 * @brief Routines for the Poll Control Server plugin, which implement the
 *        server side of the Poll Control cluster. The Poll Control cluster
 *        provides a means to communicate with an end device with a sleep
 *        schedule.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include <FreeRTOS.h>
#include <task.h>
#include "mb.h"
#include "mbport.h"
#include "modbus.h"

#include "modbus_slave.h"

#define MODBUS_COILS_NUMBER_OF_SWITCH	(8)
#define MODBUS_REG_COILS_START			(1)
//#define MODBUS_REG_COILS_END			(9999)
#define MODBUS_REG_COILS_END			(MODBUS_REG_COILS_START + (MODBUS_COILS_NUMBER_OF_SWITCH * 2))
#define MODBUS_REG_COILS_NREGS     		(MODBUS_REG_COILS_END-MODBUS_REG_COILS_START)

#define MODBUS_REG_DISCRETE_START		(10001)
#define MODBUS_REG_DISCRETE_END			(19999)
#define MODBUS_REG_DISCRETE_NREGS		(MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

#define MODBUS_REG_HOLDING_START		(40001)
#define MODBUS_REG_HOLDING_END			(49999)
#define MODBUS_REG_HOLDING_NREGS		(MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

#define MODBUS_REG_INPUT_START			(30001)
#define MODBUS_REG_INPUT_END			(39999)
#define MODBUS_REG_INPUT_NREGS			(MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

#define MB_TASK_STACK_SIZE 256u
#define MB_TASK__PRIORITY 3u
static TaskHandle_t mb_slave_task_handle;
static StackType_t mb_task_stack[MB_TASK_STACK_SIZE];
static StaticTask_t mb_task_tcb;
static void mb_slave_task(void *argument);
static const UCHAR slv_idstr[] = { 0xAA, 0xBB, 0xCC };

#if (MODBUS_REG_COILS_NREGS % 8)
#define MODBUS_REG_COILS_U8_SIZE  ((MODBUS_REG_COILS_NREGS / 8) + 1)
#else
#define MODBUS_REG_COILS_U8_SIZE  (MODBUS_REG_COILS_NREGS / 8)
#endif
static uint8_t coils_buffer[MODBUS_REG_COILS_U8_SIZE];
static uint8_t coils_cache_buffer[MODBUS_REG_COILS_U8_SIZE];

static void mb_slave_task(void *argument)
{
	mb_err_enum status;
	mb_inst_struct *inst;

	inst = mb_get_instance(0);
	if (!inst)
	{
		while(1);
	}

	status = modbus_transport_init(
				0,
				MB_RTU, // mb_mode_enum mode,
				FALSE,  // BOOL is_master,
				0xa, //UCHAR slv_addr,
				9600, // ULONG baud,
				MB_PAR_NONE // mb_port_ser_parity_enum parity
			);

	status = modbus_set_slv_id(0, 0x34, // UCHAR slv_id,
				TRUE, // BOOL is_running,
				slv_idstr,  // UCHAR const *slv_idstr,
				3   // USHORT slv_idstr_len
			);

	status = modbus_enable(0);

	while (1)
	{
		status = mb_poll(inst);
	}
}

static void coils_register_changed_callback(uint16_t coils_address, uint8_t value)
{

}

static void coils_register_changed_handler(void)
{
	uint16_t i, j;

	for (i = 0; i < MODBUS_REG_COILS_U8_SIZE; i++)
	{
		uint8_t mask;

		mask = coils_buffer[i] ^ coils_cache_buffer[i];
		if (!mask)
		{
			continue;
		}
		coils_cache_buffer[i] = coils_buffer[i];

		for (j = 0; j < 8; j++)
		{
			if (mask & (1 << j))
			{
				coils_register_changed_callback((i * 8) + j, (coils_buffer[i] & (1 << j)) ? 1 : 0);
			}
		}
	}
}

mb_err_enum modbus_slave_init(void)
{
	mb_slave_task_handle = xTaskCreateStatic(mb_slave_task,
									   "mb_slave_task",
									   MB_TASK_STACK_SIZE,
									   NULL,
									   MB_TASK__PRIORITY,
									   mb_task_stack,
									   &mb_task_tcb);
}

mb_err_enum
mb_reg_input_cb(mb_inst_struct *inst, UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
	static uint16_t input_register_counter=0;
	mb_err_enum    m_status = MB_ENOERR;

	if( ( usAddress >= MODBUS_REG_INPUT_START ) &&
		( usAddress + usNRegs <= MODBUS_REG_INPUT_START + MODBUS_REG_INPUT_NREGS ) )
	{
		input_register_counter++;
		*pucRegBuffer++ = input_register_counter >> 8;
		*pucRegBuffer++ = input_register_counter & 0xff;
	}
	else
	{
		m_status = MB_ENOREG;
	}
	return m_status;
}
mb_err_enum mb_reg_holding_cb(mb_inst_struct *inst, UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNRegs, mb_reg_mode_enum eMode)
{
	static uint16_t holding_register_counter=0;
	mb_err_enum    m_status = MB_ENOERR;
	USHORT         iRegIndex;

	if( ( usAddress >= MODBUS_REG_HOLDING_START ) &&
		( usAddress + usNRegs <= MODBUS_REG_HOLDING_START + MODBUS_REG_HOLDING_NREGS ) )
	{
		iRegIndex = ( int )( usAddress - MODBUS_REG_HOLDING_START );
		switch ( eMode )
		{
			/* Pass current register values to the protocol stack. */
		case MB_REG_READ:
			holding_register_counter++;
			*pucRegBuffer++ = holding_register_counter >> 8;
			*pucRegBuffer++ = holding_register_counter & 0xff;
			break;

		case MB_REG_WRITE:
			holding_register_counter = *pucRegBuffer++ << 8;
			holding_register_counter |= *pucRegBuffer++;

		default:
			m_status = MB_EINVAL;
			break;

		}
	}
	else
	{
		m_status = MB_ENOREG;
	}
	return m_status;
}

mb_err_enum mb_reg_coils_cb(mb_inst_struct *inst, UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNCoils, mb_reg_mode_enum eMode)
{
  mb_err_enum    m_status = MB_ENOERR;
  int             iNCoils = ( int )usNCoils;
  unsigned short  usBitOffset;

  /* Check if we have registers mapped at this block. */
  if( ( usAddress >= MODBUS_REG_COILS_START ) &&
    ( usAddress + usNCoils <= MODBUS_REG_COILS_START + MODBUS_REG_COILS_NREGS ) )
  {
    usBitOffset = ( unsigned short )( usAddress - MODBUS_REG_COILS_START );
    switch ( eMode )
    {
        /* Read current values and pass to protocol stack. */
      case MB_REG_READ:
        while( iNCoils > 0 )
        {
          *pucRegBuffer++ = mb_util_get_bits( coils_buffer, usBitOffset, ( unsigned char )(  iNCoils > 8 ? 8 : iNCoils ) );
          iNCoils -= 8;
          usBitOffset += 8;
        }
        break;

        /* Update current register values. */
      case MB_REG_WRITE:
        while( iNCoils > 0 )
        {
          mb_util_set_bits( coils_buffer, usBitOffset, ( unsigned char )( iNCoils > 8 ? 8 : iNCoils ), *pucRegBuffer++ );
          iNCoils -= 8;
          usBitOffset += 8;
        }
        coils_register_changed_handler();
        break;
    }

  }
  else
  {
    m_status = MB_ENOREG;
  }
  return m_status;
}

mb_err_enum mb_reg_discrete_cb(mb_inst_struct *inst, UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
	static uint16_t discrete_register_counter=0;
	mb_err_enum    m_status = MB_ENOERR;

	if( ( usAddress >= MODBUS_REG_DISCRETE_START ) &&
		( usAddress + usNRegs <= MODBUS_REG_DISCRETE_START + MODBUS_REG_DISCRETE_NREGS ) )
	{
		discrete_register_counter++;
		*pucRegBuffer++ = discrete_register_counter >> 8;
		*pucRegBuffer++ = discrete_register_counter & 0xff;
	}
	else
	{
		m_status = MB_ENOREG;
	}
	return m_status;
}

uint8_t modbus_coils_get_bit(uint16_t bit_offset)
{
	if(bit_offset >= MODBUS_REG_COILS_NREGS)
	{
		return 0;
	}
	return (mb_util_get_bits( coils_buffer, bit_offset, 1 ) == 0 ? 0:1);
}

void modbus_coils_set_bit(uint16_t bit_offset)
{
	if(bit_offset >= MODBUS_REG_COILS_NREGS)
	{
		return;
	}
	mb_util_set_bits(coils_buffer, bit_offset, 1, 1);
}

void modbus_coils_clear_bit(uint16_t bit_offset)
{
	if(bit_offset >= MODBUS_REG_COILS_NREGS)
	{
		return;
	}
	mb_util_set_bits(coils_buffer, bit_offset, 1, 0);
}

void modbus_coils_toggle_bit(uint16_t bit_offset)
{
	UCHAR bit_val;

	if(bit_offset >= MODBUS_REG_COILS_NREGS)
	{
		return;
	}
	bit_val = mb_util_get_bits( coils_buffer, bit_offset, 1 );
	mb_util_set_bits(coils_buffer, bit_offset, 1, bit_val == 0 ? 1:0);
}

