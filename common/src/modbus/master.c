/***************************************************************************//**
 * @file
 * @brief Routines for the Poll Control Server plugin, which implement the
 *        server side of the Poll Control cluster. The Poll Control cluster
 *        provides a means to communicate with an end device with a sleep
 *        schedule.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include <FreeRTOS.h>
#include <task.h>
#include "mb.h"
#include "mbport.h"
#include "modbus.h"

#define delay(ms)	vTaskDelay(ms/portTICK_RATE_MS);

#define SLAVE_ADDRESS (0xa)

#define MODBUS_COILS_NUMBER_OF_SWITCH (8)
#define MODBUS_REG_COILS_START        (1)
//#define MODBUS_REG_COILS_END      (9999)
#define MODBUS_REG_COILS_END          (MODBUS_REG_COILS_START + (MODBUS_COILS_NUMBER_OF_SWITCH * 2))
#define MODBUS_REG_COILS_NREGS        (MODBUS_REG_COILS_END-MODBUS_REG_COILS_START)

#define MODBUS_REG_DISCRETE_START   (10001)
#define MODBUS_REG_DISCRETE_END     (19999)
#define MODBUS_REG_DISCRETE_NREGS   (MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

#define MODBUS_REG_HOLDING_START    (40001)
#define MODBUS_REG_HOLDING_END      (49999)
#define MODBUS_REG_HOLDING_NREGS    (MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

#define MODBUS_REG_INPUT_START      (30001)
#define MODBUS_REG_INPUT_END      (39999)
#define MODBUS_REG_INPUT_NREGS      (MODBUS_REG_HOLDING_END-MODBUS_REG_HOLDING_START)

//#if MB_MASTER > 0

static mb_inst_struct *mb_inst;

#define MB_TASK_STACK_SIZE 256u
#define MB_TASK__PRIORITY 3u
static TaskHandle_t mb_polling_task_handle;
static StackType_t mb_polling_task_stack[MB_TASK_STACK_SIZE];
static StaticTask_t mb_polling_task_tcb;

static TaskHandle_t mb_master_task_handle;
static StackType_t mb_task_stack[MB_TASK_STACK_SIZE];
static StaticTask_t mb_task_tcb;

static void mb_master_task(void *argument);

enum MODBUS_ACTION
{
  MODBUS_ACTION_READ_COILS,
  MODBUS_ACTION_WRITE_COILS,
  MODBUS_ACTION_WRITE_ONE_COILS,
};

static struct
{
#define TICK_TO_MS(ticks)	( ticks * portTICK_RATE_MS )
  enum MODBUS_ACTION action;
  UCHAR slave_address;
  USHORT retry_delay_ms;
  TickType_t last_retry_tick;
  union
  {
    struct
    {
      USHORT reg_addr;
      USHORT reg_num;
    }read_coils;

    struct
    {
      USHORT reg_addr;
      USHORT coil_data;
    }write_one_coils;

    struct
    {
      USHORT reg_addr;
      USHORT coil_data;
      USHORT coil_num;
      UCHAR * data_ptr;
    }write_coils;
  };
}modbusActionParams;

static void read_all_coils(uint32_t delay)
{
	modbusActionParams.retry_delay_ms = delay;
	modbusActionParams.action=MODBUS_ACTION_READ_COILS;
	modbusActionParams.slave_address = SLAVE_ADDRESS;
	modbusActionParams.read_coils.reg_addr = MODBUS_REG_COILS_START-1;
	modbusActionParams.read_coils.reg_num = MODBUS_REG_COILS_NREGS;
}

static void mb_polling_task(void *argument)
{
	mb_err_enum m_status;

	mb_inst = mb_get_instance(0);
	if (!mb_inst)
	{
		while(1);
	}

	m_status = modbus_transport_init(
				0,
				MB_RTU, // mb_mode_enum mode,
				TRUE,  // BOOL is_master,
				SLAVE_ADDRESS, //UCHAR slv_addr,
				9600, // ULONG baud,
				MB_PAR_NONE // mb_port_ser_parity_enum parity
			);

	m_status = modbus_enable(0);

	mb_master_task_handle = xTaskCreateStatic(mb_master_task,
										   "mb_master_task",
										   MB_TASK_STACK_SIZE,
										   NULL,
										   MB_TASK__PRIORITY,
										   mb_task_stack,
										   &mb_task_tcb);

	while (1)
	{
		m_status = mb_poll(mb_inst);
	}
}

static void mb_master_task(void *argument)
{
	mb_err_enum m_status;

	read_all_coils(1000);
	modbusActionParams.last_retry_tick = xTaskGetTickCount();
	while(1)
	{

		switch (modbusActionParams.action)
		{
		case MODBUS_ACTION_READ_COILS:
//			m_status = mb_mstr_rq_read_coils(mb_inst,
//					modbusActionParams.slave_address,
//					modbusActionParams.read_coils.reg_addr,
//					modbusActionParams.read_coils.reg_num);
			m_status = mb_mstr_rq_write_coil(mb_inst,
								modbusActionParams.slave_address,
								modbusActionParams.read_coils.reg_addr,
								0xff00);
			read_all_coils(1000);
			break;

		case MODBUS_ACTION_WRITE_COILS:
			m_status = mb_mstr_rq_write_multi_coils(mb_inst,
					modbusActionParams.slave_address,
					modbusActionParams.write_coils.reg_addr,
					modbusActionParams.write_coils.coil_num,
					modbusActionParams.write_coils.data_ptr);
			break;

		case MODBUS_ACTION_WRITE_ONE_COILS:
			m_status = mb_mstr_rq_write_coil(mb_inst,
					modbusActionParams.slave_address,
					modbusActionParams.write_one_coils.reg_addr,
					modbusActionParams.write_one_coils.coil_data);
			break;
		}
		modbusActionParams.last_retry_tick = xTaskGetTickCount();

		if(MB_EBUSY == m_status)
		{
		  mb_disable(mb_inst);

		  mb_enable(mb_inst);
		}

		delay(modbusActionParams.retry_delay_ms);
	}
}

mb_err_enum modbus_master_init(void)
{
	mb_polling_task_handle = xTaskCreateStatic(mb_polling_task,
									   "mb_polling_task",
									   MB_TASK_STACK_SIZE,
									   NULL,
									   MB_TASK__PRIORITY,
									   mb_polling_task_stack,
									   &mb_polling_task_tcb);


}

mb_err_enum  mb_mstr_reg_input_cb   (mb_inst_struct *inst, UCHAR *reg_buff, USHORT reg_addr, USHORT reg_num )
{
	return MB_ENOERR;
}
mb_err_enum  mb_mstr_reg_holding_cb (mb_inst_struct *inst, UCHAR *reg_buff, USHORT reg_addr, USHORT reg_num )
{
	return MB_ENOERR;
}

mb_err_enum  mb_mstr_reg_discrete_cb(mb_inst_struct *inst, UCHAR *reg_buff, USHORT reg_addr, USHORT disc_num)
{
	return MB_ENOERR;
}
mb_err_enum  mb_mstr_reg_coils_cb   (mb_inst_struct *inst, UCHAR *reg_buff, USHORT reg_addr, USHORT coil_num)
{
	return MB_ENOERR;
}

void mb_mstr_error_exec_fn_cb (mb_inst_struct *inst)
{

}
void mb_mstr_error_rcv_data_cb(mb_inst_struct *inst)
{

}
void mb_mstr_error_timeout_cb (mb_inst_struct *inst)
{

}
void mb_mstr_rq_success_cb(mb_inst_struct *inst)
{
	switch (modbusActionParams.action)
	{
	case MODBUS_ACTION_READ_COILS:
		read_all_coils(10);
		break;

	case MODBUS_ACTION_WRITE_COILS:
	case MODBUS_ACTION_WRITE_ONE_COILS:
		read_all_coils(10);
		break;
	}
}

//#endif // #if MB_MASTER > 0
