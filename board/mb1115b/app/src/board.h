 
#ifndef BOARD_H_
#define BOARD_H_

#if defined(STM32F0)
#include <stm32f0xx_hal.h>
#elif defined(STM32F4)
#include <stm32f4xx_hal.h>
#else
#error "Chip is not supported"
#endif

// Debug UART
#define DBG_UART_TX_PORT                GPIOA
#define DBG_UART_TX_PIN                 GPIO_PIN_2
#define DBG_UART_TX_AF					GPIO_AF7_USART2
#define DBG_UART_TX_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE

#define DBG_UART_RX_PORT                GPIOA
#define DBG_UART_RX_PIN                 GPIO_PIN_3
#define DBG_UART_RX_AF					GPIO_AF7_USART2
#define DBG_UART_RX_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE

#define DBG_UART						USART2
#define DBG_UART_CLK_ENABLE				__HAL_RCC_USART2_CLK_ENABLE
#define DBG_UART_IRQn					USART2_IRQn
#define DBG_UART_IRQHandler				USART2_IRQHandler


// Board I2C
#define BOARD_I2C_I2C                             I2C1
#define BOARD_I2C_I2C_CLK_ENABLE()                __HAL_RCC_I2C1_CLK_ENABLE()
#define BOARD_I2C_I2C_SDA_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define BOARD_I2C_I2C_SCL_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()

#define BOARD_I2C_I2C_FORCE_RESET()               __HAL_RCC_I2C1_FORCE_RESET()
#define BOARD_I2C_I2C_RELEASE_RESET()             __HAL_RCC_I2C1_RELEASE_RESET()

/* Definition for BOARD_I2C_I2C Pins */
#define BOARD_I2C_I2C_SCL_PIN                    GPIO_PIN_6
#define BOARD_I2C_I2C_SCL_GPIO_PORT              GPIOB
#define BOARD_I2C_I2C_SCL_AF                     GPIO_AF4_I2C1
#define BOARD_I2C_I2C_SDA_PIN                    GPIO_PIN_9
#define BOARD_I2C_I2C_SDA_GPIO_PORT              GPIOB
#define BOARD_I2C_I2C_SDA_AF                     GPIO_AF4_I2C1


// Led
#define LED3_PORT                GPIOD
#define LED3_PIN                 GPIO_PIN_13
#define LED3_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED4_PORT                GPIOD
#define LED4_PIN                 GPIO_PIN_12
#define LED4_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED5_PORT                GPIOD
#define LED5_PIN                 GPIO_PIN_14
#define LED5_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED6_PORT                GPIOD
#define LED6_PIN                 GPIO_PIN_15
#define LED6_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE



// Definition for TIMx
#define MB_TIMER							TIM11
#define MB_TIMER_CLK_ENABLE					__HAL_RCC_TIM11_CLK_ENABLE
#define MB_TIMER_IRQn						TIM1_TRG_COM_TIM11_IRQn
#define MB_TIMER_IRQHandler					TIM1_TRG_COM_TIM11_IRQHandler
// Definition for UART
#define MB_UART							USART1
#define MB_UART_CLK_ENABLE				__HAL_RCC_USART1_CLK_ENABLE
#define MB_UART_IRQn					USART1_IRQn
#define MB_UART_IRQHandler				USART1_IRQHandler

#define MB_UART_TX_PORT					GPIOB
#define MB_UART_TX_PIN					GPIO_PIN_6
#define MB_UART_TX_PORT_CLK_ENABLE		__HAL_RCC_GPIOB_CLK_ENABLE
#define MB_UART_TX_AF					GPIO_AF7_USART1

#define MB_UART_RX_PORT					GPIOB
#define MB_UART_RX_PIN					GPIO_PIN_7
#define MB_UART_RX_PORT_CLK_ENABLE		__HAL_RCC_GPIOB_CLK_ENABLE
#define MB_UART_RX_AF					GPIO_AF7_USART1

//#define MB_RS485_RE_PORT                GPIOA
//#define MB_RS485_RE_PIN                 GPIO_PIN_9
//#define MB_RS485_RE_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE
//
//#define MB_RS485_DE_PORT                GPIOA
//#define MB_RS485_DE_PIN                 GPIO_PIN_9
//#define MB_RS485_DE_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE

#define LED_PORT						GPIOD
#define LED_PIN							GPIO_PIN_12
#define LED_PORT_CLK_ENABLE				__HAL_RCC_GPIOD_CLK_ENABLE



extern UART_HandleTypeDef board_dbg_uart_handle;
extern UART_HandleTypeDef mb_uart_handle;

UART_HandleTypeDef *board_mb_uart_init(uint16_t baud,
						uint16_t word_length,
						uint16_t stop_bits,
						uint16_t parity);

void board_init(void);

#endif
