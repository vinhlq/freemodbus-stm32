#include <stm32l1xx_hal.h>

static void SystemClockHSI_Config(void);
static void SystemClockHSE_Config(void);

#define LED1_PORT                GPIOD
#define LED1_PIN                 GPIO_PIN_0
#define LED1_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED2_PORT                GPIOD
#define LED2_PIN                 GPIO_PIN_1
#define LED2_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED3_PORT                GPIOD
#define LED3_PIN                 GPIO_PIN_2
#define LED3_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED4_PORT                GPIOD
#define LED4_PIN                 GPIO_PIN_3
#define LED4_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED5_PORT                GPIOD
#define LED5_PIN                 GPIO_PIN_4
#define LED5_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

void initGPIO()
{
    GPIO_InitTypeDef GPIO_Config;

    GPIO_Config.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Config.Pull = GPIO_NOPULL;
    GPIO_Config.Speed = GPIO_SPEED_FREQ_HIGH;

    LED1_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED1_PIN;
	HAL_GPIO_Init(LED1_PORT, &GPIO_Config);
	HAL_GPIO_WritePin(LED1_PORT, LED1_PIN, GPIO_PIN_RESET);

	LED2_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED2_PIN;
	HAL_GPIO_Init(LED1_PORT, &GPIO_Config);
	HAL_GPIO_WritePin(LED2_PORT, LED2_PIN, GPIO_PIN_RESET);

	LED3_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED3_PIN;
	HAL_GPIO_Init(LED3_PORT, &GPIO_Config);
	HAL_GPIO_WritePin(LED3_PORT, LED3_PIN, GPIO_PIN_RESET);

	LED4_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED4_PIN;
	HAL_GPIO_Init(LED4_PORT, &GPIO_Config);
	HAL_GPIO_WritePin(LED4_PORT, LED4_PIN, GPIO_PIN_RESET);

	LED5_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED5_PIN;
	HAL_GPIO_Init(LED5_PORT, &GPIO_Config);
	HAL_GPIO_WritePin(LED5_PORT, LED5_PIN, GPIO_PIN_RESET);
}

int main(void)
{
	HAL_Init();
//    SystemClockHSE_Config();
    SystemClockHSI_Config();

    initGPIO();

    for (;;)
    {
    	HAL_GPIO_TogglePin(LED1_PORT, LED1_PIN);
    	HAL_GPIO_TogglePin(LED2_PORT, LED2_PIN);
    	HAL_GPIO_TogglePin(LED3_PORT, LED3_PIN);
    	HAL_GPIO_TogglePin(LED4_PORT, LED4_PIN);
    	HAL_GPIO_TogglePin(LED5_PORT, LED5_PIN);
    	HAL_Delay(500);
    }
    
    return 0;
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 32000000
  *            HCLK(Hz)                       = 32000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSI Frequency(Hz)              = 16000000
  *            PLLMUL                         = 6
  *            PLLDIV                         = 3
  *            Flash Latency(WS)              = 1
  * @retval None
  */
void SystemClockHSI_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* Enable HSE Oscillator and Activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV          = RCC_PLL_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
	while(1);
  }

  /* Set Voltage scale1 as MCU will run at 32MHz */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Poll VOSF bit of in PWR_CSR. Wait until it is reset to 0 */
  while (__HAL_PWR_GET_FLAG(PWR_FLAG_VOS) != RESET) {};

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
  clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    while(1);
  }
}
