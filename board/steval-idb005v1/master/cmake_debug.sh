#!/bin/sh

set -e

export env $(cat .env | grep -v '^#' | xargs)

BOARD_NAME=$(basename -- $(dirname $(dirname $(readlink -f $0))))
VARIANT_NAME=$(basename -- $(dirname $(readlink -f $0)))

PROJECT_BASE_DIR=$PWD
BUILD_DIR=$PWD/../../../build/${BOARD_NAME}/${VARIANT_NAME}

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
-DBOARD_NAME=${BOARD_NAME}  \
-DSTM32_TOOLCHAIN_PATH=${STM32_TOOLCHAIN_PATH}  \
-DCMAKE_BUILD_TYPE=Debug \
-G "Eclipse CDT4 - Unix Makefiles" \
$PROJECT_BASE_DIR
