 
#ifndef BOARD_H_
#define BOARD_H_

#include <stm32l1xx_hal.h>

// Debug UART
#define DBG_UART_TX_PORT                GPIOB
#define DBG_UART_TX_PIN                 GPIO_PIN_10
#define DBG_UART_TX_AF					GPIO_AF7_USART3
#define DBG_UART_TX_PORT_CLK_ENABLE     __HAL_RCC_GPIOB_CLK_ENABLE

#define DBG_UART_RX_PORT                GPIOB
#define DBG_UART_RX_PIN                 GPIO_PIN_11
#define DBG_UART_RX_AF					GPIO_AF7_USART3
#define DBG_UART_RX_PORT_CLK_ENABLE     __HAL_RCC_GPIOB_CLK_ENABLE

#define DBG_UART						USART3
#define DBG_UART_CLK_ENABLE				__HAL_RCC_USART3_CLK_ENABLE
#define DBG_UART_IRQn					USART3_IRQn
#define DBG_UART_IRQHandler				USART3_IRQHandler


// Board I2C
#define BOARD_I2C_I2C                             I2C1
#define BOARD_I2C_I2C_CLK_ENABLE()                __HAL_RCC_I2C1_CLK_ENABLE()
#define BOARD_I2C_I2C_SDA_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define BOARD_I2C_I2C_SCL_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()

#define BOARD_I2C_I2C_FORCE_RESET()               __HAL_RCC_I2C1_FORCE_RESET()
#define BOARD_I2C_I2C_RELEASE_RESET()             __HAL_RCC_I2C1_RELEASE_RESET()

/* Definition for BOARD_I2C_I2C Pins */
#define BOARD_I2C_I2C_SCL_PIN                    GPIO_PIN_6
#define BOARD_I2C_I2C_SCL_GPIO_PORT              GPIOB
#define BOARD_I2C_I2C_SCL_AF                     GPIO_AF4_I2C1
#define BOARD_I2C_I2C_SDA_PIN                    GPIO_PIN_9
#define BOARD_I2C_I2C_SDA_GPIO_PORT              GPIOB
#define BOARD_I2C_I2C_SDA_AF                     GPIO_AF4_I2C1


// Led
#define LED1_PORT                GPIOC
#define LED1_PIN                 GPIO_PIN_0
#define LED1_PORT_CLK_ENABLE     __HAL_RCC_GPIOC_CLK_ENABLE

#define LED2_PORT                GPIOC
#define LED2_PIN                 GPIO_PIN_1
#define LED2_PORT_CLK_ENABLE     __HAL_RCC_GPIOC_CLK_ENABLE

#define LED3_PORT                GPIOD
#define LED3_PIN                 GPIO_PIN_2
#define LED3_PORT_CLK_ENABLE     __HAL_RCC_GPIOD_CLK_ENABLE

#define LED4_PORT                GPIOC
#define LED4_PIN                 GPIO_PIN_4
#define LED4_PORT_CLK_ENABLE     __HAL_RCC_GPIOC_CLK_ENABLE

#define LED5_PORT                GPIOC
#define LED5_PIN                 GPIO_PIN_5
#define LED5_PORT_CLK_ENABLE     __HAL_RCC_GPIOC_CLK_ENABLE



// Definition for TIMx
// Definition for TIMx
#define MB_TIMER						TIM7
#define MB_TIMER_CLK_ENABLE				__HAL_RCC_TIM7_CLK_ENABLE
#define MB_TIMER_IRQn					TIM7_IRQn
#define MB_TIMER_IRQHandler				TIM7_IRQHandler
// Definition for UART
#define MB_UART							USART2
#define MB_UART_CLK_ENABLE				__HAL_RCC_USART2_CLK_ENABLE
#define MB_UART_IRQn					USART2_IRQn
#define MB_UART_IRQHandler				USART2_IRQHandler

#define MB_UART_TX_PORT					GPIOA
#define MB_UART_TX_PIN					GPIO_PIN_2
#define MB_UART_TX_PORT_CLK_ENABLE		__HAL_RCC_GPIOA_CLK_ENABLE
#define MB_UART_TX_AF					GPIO_AF7_USART2

#define MB_UART_RX_PORT					GPIOA
#define MB_UART_RX_PIN					GPIO_PIN_3
#define MB_UART_RX_PORT_CLK_ENABLE		__HAL_RCC_GPIOA_CLK_ENABLE
#define MB_UART_RX_AF					GPIO_AF7_USART2

//#define MB_RS485_RE_PORT                GPIOA
//#define MB_RS485_RE_PIN                 GPIO_PIN_9
//#define MB_RS485_RE_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE
//
//#define MB_RS485_DE_PORT                GPIOA
//#define MB_RS485_DE_PIN                 GPIO_PIN_9
//#define MB_RS485_DE_PORT_CLK_ENABLE     __HAL_RCC_GPIOA_CLK_ENABLE

#define MB_LED_PORT						GPIOC
#define MB_LED_PIN						GPIO_PIN_0
#define MB_LED_PORT_CLK_ENABLE			__HAL_RCC_GPIOC_CLK_ENABLE



extern UART_HandleTypeDef board_dbg_uart_handle;
extern UART_HandleTypeDef mb_uart_handle;

UART_HandleTypeDef *board_mb_uart_init(uint16_t baud,
						uint16_t word_length,
						uint16_t stop_bits,
						uint16_t parity);

void board_init(void);

#endif
