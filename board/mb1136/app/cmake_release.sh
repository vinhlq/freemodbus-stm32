#!/bin/sh

export env $(cat .env | grep -v '^#' | xargs)

PROJECT_BASE_DIR=$PWD
BUILD_DIR=$PWD/../../../build/mb1136/app

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
-DSTM32_TOOLCHAIN_PATH=${STM32_TOOLCHAIN_PATH}  \
-DCMAKE_BUILD_TYPE=Release \
-G "Eclipse CDT4 - Unix Makefiles" \
$PROJECT_BASE_DIR
