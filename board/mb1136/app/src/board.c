

#include "board.h"

UART_HandleTypeDef mb_uart_handle;
UART_HandleTypeDef board_dbg_uart_handle;

void board_dbg_uart_init(void)
{
	GPIO_InitTypeDef GPIO_Config;

	DBG_UART_TX_PORT_CLK_ENABLE();
	DBG_UART_RX_PORT_CLK_ENABLE();
	/* Enable USARTx clock */
	DBG_UART_CLK_ENABLE();

	GPIO_Config.Mode = GPIO_MODE_AF_PP;
	GPIO_Config.Pull = GPIO_PULLUP;
	GPIO_Config.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_Config.Alternate = DBG_UART_TX_AF;
	GPIO_Config.Pin = DBG_UART_TX_PIN;
	HAL_GPIO_Init(DBG_UART_TX_PORT, &GPIO_Config);

	GPIO_Config.Alternate = DBG_UART_RX_AF;
	GPIO_Config.Pin = DBG_UART_RX_PIN;
	HAL_GPIO_Init(DBG_UART_RX_PORT, &GPIO_Config);

	board_dbg_uart_handle.Instance = DBG_UART;
	board_dbg_uart_handle.Init.BaudRate = 115200;
	board_dbg_uart_handle.Init.WordLength = UART_WORDLENGTH_8B;
	board_dbg_uart_handle.Init.StopBits = UART_STOPBITS_1;
	board_dbg_uart_handle.Init.Parity = UART_PARITY_NONE;
	board_dbg_uart_handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	board_dbg_uart_handle.Init.Mode = UART_MODE_TX_RX;
	board_dbg_uart_handle.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_DeInit(&board_dbg_uart_handle) != HAL_OK)
	{
		while(1);
	}
	if (HAL_UART_Init(&board_dbg_uart_handle) != HAL_OK)
	{
		while(1);
	}

	/*##-3- Configure the NVIC for UART ########################################*/
	/* NVIC for USART */
//	HAL_NVIC_SetPriority(DBG_UART_IRQn, 1, 0);
//	HAL_NVIC_EnableIRQ(DBG_UART_IRQn);
}

UART_HandleTypeDef *board_mb_uart_init(uint16_t baud,
						uint16_t word_length,
						uint16_t stop_bits,
						uint16_t parity)
{
	GPIO_InitTypeDef GPIO_Config;

	MB_UART_TX_PORT_CLK_ENABLE();
	MB_UART_RX_PORT_CLK_ENABLE();
	/* Enable USARTx clock */
	MB_UART_CLK_ENABLE();

	GPIO_Config.Mode = GPIO_MODE_AF_PP;
	GPIO_Config.Pull = GPIO_PULLUP;
	GPIO_Config.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_Config.Alternate = MB_UART_TX_AF;
	GPIO_Config.Pin = MB_UART_TX_PIN;
	HAL_GPIO_Init(MB_UART_TX_PORT, &GPIO_Config);

	GPIO_Config.Alternate = MB_UART_RX_AF;
	GPIO_Config.Pin = MB_UART_RX_PIN;
	HAL_GPIO_Init(MB_UART_RX_PORT, &GPIO_Config);

	mb_uart_handle.Instance = MB_UART;
	mb_uart_handle.Init.BaudRate = 9600;
	mb_uart_handle.Init.WordLength = UART_WORDLENGTH_8B;
	mb_uart_handle.Init.StopBits = UART_STOPBITS_1;
	mb_uart_handle.Init.Parity = UART_PARITY_NONE;
	mb_uart_handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	mb_uart_handle.Init.Mode = UART_MODE_TX_RX;
#if defined(STM32F0)
	mb_uart_handle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
#endif
	if (HAL_UART_DeInit(&mb_uart_handle) != HAL_OK)
	{
		while(1);
	}
	if (HAL_UART_Init(&mb_uart_handle) != HAL_OK)
	{
		while(1);
	}

	/*##-3- Configure the NVIC for UART ########################################*/
	/* NVIC for USART */
	HAL_NVIC_SetPriority(MB_UART_IRQn, 10, 0);
	HAL_NVIC_EnableIRQ(MB_UART_IRQn);

	return &mb_uart_handle;
}

void board_led_init(void)
{
	GPIO_InitTypeDef GPIO_Config;

	GPIO_Config.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_Config.Pull = GPIO_NOPULL;
	GPIO_Config.Speed = GPIO_SPEED_FREQ_HIGH;

	LED2_PORT_CLK_ENABLE();
	GPIO_Config.Pin = LED2_PIN;
	HAL_GPIO_Init(LED2_PORT, &GPIO_Config);

	HAL_GPIO_WritePin(LED2_PORT, LED2_PIN, GPIO_PIN_RESET);
}

void board_init(void)
{
	board_dbg_uart_init();

//	board_i2c_init();

	board_led_init();
}
