#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>

#include "board.h"

#include "wrap_printf.h"

#include "mb.h"
#include "mbport.h"
#include "modbus.h"

static void SystemClockHSI_Config(void);
static void SystemClockHSE_Config(void);

#define MB_TASK_STACK_SIZE 130u
#define MB_TASK__PRIORITY 3u
static StackType_t mb_task_stack[MB_TASK_STACK_SIZE];
static StaticTask_t mb_task_tcb;

static void blink_task(void *arg)
{
	for (;;)
	{
		vTaskDelay(500);
		HAL_GPIO_TogglePin(LED2_PORT, LED2_PIN);
//		printf("Hello world!!");

		if (HAL_UART_Transmit(&board_dbg_uart_handle, (uint8_t*) "123\r\n", 5, HAL_MAX_DELAY) != HAL_OK)
		{
			while(1);
		}

//		if (HAL_UART_Transmit(&mb_uart_handle, (uint8_t*) "456\r\n", 5, HAL_MAX_DELAY) != HAL_OK)
//		{
//			while(1);
//		}
	}
}

int main(void)
{
    SystemInit();

    HAL_Init();

//	SystemClockHSE_Config();
	SystemClockHSI_Config();

    SystemCoreClockUpdate();
	HAL_InitTick(TICK_INT_PRIORITY);

	board_init();
    
    wrap_printf_set_uart_handle(&board_dbg_uart_handle);
    
    xTaskCreateStatic(blink_task,
					   "blinky",
					   MB_TASK_STACK_SIZE,
					   NULL,
					   MB_TASK__PRIORITY,
					   mb_task_stack,
					   &mb_task_tcb);

    modbus_slave_init();
    modbus_slave_start();

//    modbus_master_init();

    vTaskStartScheduler();
    for (;;);
    
    return 0;
}

void vApplicationTickHook(void)
{
}

void vApplicationIdleHook(void)
{
}

void vApplicationMallocFailedHook(void)
{
    taskDISABLE_INTERRUPTS();
    for(;;);
}

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
    (void) pcTaskName;
    (void) pxTask;

    taskDISABLE_INTERRUPTS();
    for(;;);
}

void vMainAssertCalled( const char *pcFileName, uint32_t ulLineNumber )
{
	xil_printf( "ASSERT!  Line %lu of file %s\r\n", ulLineNumber, pcFileName );
	taskENTER_CRITICAL();
	for( ;; );
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSI/2)
  *            SYSCLK(Hz)                     = 48000000
  *            HCLK(Hz)                       = 48000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            HSI Frequency(Hz)              = 8000000
  *            PREDIV                         = 1
  *            PLLMUL                         = 12
  *            Flash Latency(WS)              = 1
  * @param  None
  * @retval None
  */
void SystemClockHSI_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* No HSE Oscillator on Nucleo, Activate PLL with HSI/2 as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_NONE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}


