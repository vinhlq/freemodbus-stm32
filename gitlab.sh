#!/bin/sh

chmod 400 gitlab.ssh.rsa.pem
eval $(ssh-agent -s)
ssh-add gitlab.ssh.rsa.pem
git $@
ssh-agent -k
