#!/bin/sh

PWD=$(readlink -e $(dirname $0))

docker build -t ubuntu:18.04-modbustool .
docker stop ubuntu-18.04-modbustool
docker rm ubuntu-18.04-modbustool
# docker run -ti --name=ubuntu-18.04-modbustool --privileged -v /dev/bus/usb:/dev/bus/usb -w ${PWD} ubuntu:18.04-modbustool /bin/bash
docker run -ti --name=ubuntu-18.04-modbustool --device=/dev/ttyUSB0 -v /mnt:/mnt -w ${PWD} ubuntu:18.04-modbustool /bin/bash
