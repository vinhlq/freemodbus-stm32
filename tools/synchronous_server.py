#!/usr/bin/env python
"""
Pymodbus Synchronous Server Example
--------------------------------------------------------------------------

The synchronous server is implemented in pure python without any third
party libraries (unless you need to use the serial protocols which require
pyserial). This is helpful in constrained or old environments where using
twisted is just not feasible. What follows is an example of its use:
"""
# --------------------------------------------------------------------------- #
# import the various server implementations
# --------------------------------------------------------------------------- #
from pymodbus.server.sync import StartTcpServer
from pymodbus.server.sync import StartTlsServer
from pymodbus.server.sync import StartUdpServer
from pymodbus.server.sync import StartSerialServer
from pymodbus.server.sync import ModbusSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from pymodbus.transaction import ModbusRtuFramer, ModbusBinaryFramer

# --------------------------------------------------------------------------- #
# import the python libraries we need
# --------------------------------------------------------------------------- #
# from multiprocessing import Queue, Process, Lock
from threading import Thread, Lock

import functools
import asyncio
def force_async(fn):
    '''
    turns a sync function to async function using threads
    '''
    from concurrent.futures import ThreadPoolExecutor
    import asyncio
    pool = ThreadPoolExecutor()

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        future = pool.submit(fn, *args, **kwargs)
        return asyncio.wrap_future(future)  # make it awaitable

    return wrapper

def force_sync(fn):
    '''
    turn an async function to sync function
    '''
    import asyncio

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        res = fn(*args, **kwargs)
        if asyncio.iscoroutine(res):
            return asyncio.get_event_loop().run_until_complete(res)
        return res

    return wrapper


# --------------------------------------------------------------------------- #
# configure the service logging
# --------------------------------------------------------------------------- #
import logging
FORMAT = ('%(pathname)s %(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
# log.setLevel(logging.DEBUG)
log.setLevel(logging.FATAL)

import struct
import time
import curses
from collections import OrderedDict
from texttable import Texttable
import sys
import serial
import configparser
config={}
configparser = configparser.RawConfigParser()
configparser.read(r'config')
if len(sys.argv) >= 2:
    config["port"] = sys.argv[1]
else:
    config["port"]=configparser.get('serial', 'port');
if config["port"] == None:
    config["port"]='/dev/ttyUSB0'
config["baudrate"]=int(configparser.get('serial', 'baudrate'));
if config["baudrate"] == 0 or config["baudrate"] == None:
    config["baudrate"]=9600
config["timeout"]=float(configparser.get('serial', 'timeout'));
if config["timeout"] == None:
    config["timeout"]=10
parity=str(configparser.get('serial', 'parity'))
if parity == None:
    config["parity"]=serial.PARITY_NONE
elif parity.lower() == 'even':
    config["parity"]=serial.PARITY_EVEN
elif parity.lower() == 'odd':
    config["parity"]=serial.PARITY_ODD
elif parity.lower() == 'mark':
    config["parity"]=serial.PARITY_MARK
elif parity.lower() == 'space':
    config["parity"]=serial.PARITY_SPACE
else:
    config["parity"]=serial.PARITY_NONE
    
config["parity"]=serial.PARITY_NONE
    
config["col1_size"]=int(configparser.get('texttable', 'col1_size'))
if config["col1_size"] == None:
    config["col1_size"]=25
config["col2_size"]=int(configparser.get('texttable', 'col2_size'))
if config["col2_size"] == None:
    config["col2_size"]=19
config["col3_size"]=int(configparser.get('texttable', 'col3_size'))
if config["col3_size"] == None:
    config["col3_size"]=20
config["col4_size"]=int(configparser.get('texttable', 'col4_size'))
if config["col4_size"] == None:
    config["col4_size"]=8
config["col5_size"]=int(configparser.get('texttable', 'col5_size'))
if config["col5_size"] == None:
    config["col5_size"]=27
config["col6_size"]=int(configparser.get('texttable', 'col6_size'))
if config["col6_size"] == None:
    config["col6_size"]=27
config["col7_size"]=int(configparser.get('texttable', 'col7_size'))
if config["col7_size"] == None:
    config["col7_size"]=27
config["col8_size"]=int(configparser.get('texttable', 'col8_size'))
if config["col8_size"] == None:
    config["col8_size"]=27

COILS_OFFSET=int(configparser.get('modbus', 'COILS_OFFSET'))
DISCRETE_INPUT_OFFSET=int(configparser.get('modbus', 'DISCRETE_INPUT_OFFSET'))
HOLDING_OFFSET=int(configparser.get('modbus', 'HOLDING_OFFSET'))
INPUT_OFFSET=int(configparser.get('modbus', 'INPUT_OFFSET'))
UNIT=int(configparser.get('modbus', 'UNIT'))

def bits_to_u32be(bits):
    u32be = 0
    for i in range(len(bits)):
        if(bits[i]):
            u32be |= 1<<i
    return u32be
def registers_to_u16be(registers):
    (u16be,) = struct.unpack('>h', struct.pack('>H', registers[0]))
    return u16be
def bits_to_string8(u8):
    return '0b' + bin(u8%256).lstrip('-0b').zfill(8)
def bits_to_string(s):
    return str(s) if s<=1 else bits_to_string(s>>1) + str(s&1)

def modbus_make_table(coils_bits=None, discrete_input_r=None, input_r=None, holding_r=None):
    coils_d = OrderedDict()
    discrete_input_d = OrderedDict()
    holding_d = OrderedDict()
    input_d = OrderedDict()

    if(coils_bits):
        coils_d["%d(%d)" % (COILS_OFFSET, len(coils_bits))] =               (COILS_OFFSET, "", "%s" % (bits_to_string8(bits_to_u32be(coils_bits))), None)
    if(input_r):
        input_d["%d(%d)" % (INPUT_OFFSET, len(input_r))] =        (INPUT_OFFSET, "", registers_to_u16be(input_r[0:]), None)
    if(holding_r):
        holding_d["%d(%d)" % (HOLDING_OFFSET, len(holding_r))] =  (HOLDING_OFFSET, "", registers_to_u16be(holding_r[0:]), None)
    
    
    table_data = [[ "coils", "value",
                    "discrete_input", "value",
                    "input", "value",
                    "holding", "value"]]
    
    coils_items=list(coils_d.items())
    discrete_input_items=list(discrete_input_d.items())
    input_items=list(input_d.items())
    holding_items=list(holding_d.items())
    table_len = len(input_items)
    if table_len < len(coils_items):
        table_len = len(coils_items)
    if table_len < len(discrete_input_items):
        table_len = len(discrete_input_items)
    if table_len < len(input_items):
        table_len = len(input_items)
    if table_len < len(holding_items):
        table_len = len(holding_items)
        
    for i in range(table_len):
        table_data.append([])
        st = "----------------------------------------"
        
        if i < len(coils_items):
            (key,value)=coils_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col1_size"], st))
                table_data[i+1].append("%.*s" % (config["col2_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')

        if i < len(discrete_input_items):
            (key,value)=discrete_input_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col3_size"], st))
                table_data[i+1].append("%.*s" % (config["col4_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')
            
        if i < len(input_items):
            (key,value)=input_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col5_size"], st))
                table_data[i+1].append("%.*s" % (config["col6_size"], st))
            else:
                (_,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')


        if i < len(holding_items):
            (key,value)=holding_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col7_size"], st))
                table_data[i+1].append("%.*s" % (config["col8_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = value
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')

    return table_data

class CustomDataBlock(ModbusSparseDataBlock):
    """ A datablock that stores the new value in memory
    and performs a custom action after it has been stored.
    """

    def __init__(self, values):
        ''' Initializes the datastore

        Using the input values we create the default
        datastore value and the starting address

        :param values: Either a list or a dictionary of values
        '''
        super(CustomDataBlock, self).__init__(values)

    def setValues(self, address, value):
        """ Sets the requested values of the datastore

        :param address: The starting address
        :param values: The new values to be set
        """
        super(CustomDataBlock, self).setValues(address, value)

        # whatever you want to do with the written value is done here,
        # however make sure not to do too much work here or it will
        # block the server, espectially if the server is being written
        # to very quickly
        print("wrote {} to {}".format(value, address))

    def getValues(self, address, count=1):
        ''' Returns the requested values of the datastore

        :param address: The starting address
        :param count: The number of values to retrieve
        :returns: The requested values from a:a+c
        '''

        # global modbus_coils_bits
        # global modbus_lock
        # modbus_lock.acquire()
        # result = [modbus_coils_bits[i] for i in range(address, address + count)]
        # modbus_lock.release()
        return result

class CustomModbusSequentialDataBlock(ModbusSequentialDataBlock):
    def __init__(self, address, values):
        self.rCount = 0
        self.wCount = 0
        super(CustomModbusSequentialDataBlock, self).__init__(address, values)

    def getValues(self, address, count=1):
        self.rCount = self.rCount + 1
        return super(CustomModbusSequentialDataBlock, self).getValues(address, count)

    def setValues(self, address, values):
        self.wCount = self.wCount + 1
        return super(CustomModbusSequentialDataBlock, self).setValues(address, values)


class ModbusRTUSerialServer(ModbusSerialServer):
    def __init__(self, context=None, identity=None,  custom_functions=[],
                      **kwargs):
        
        # ----------------------------------------------------------------------- #
        # initialize your data store
        # ----------------------------------------------------------------------- #
        self.slaveContext = ModbusSlaveContext(
            co=CustomModbusSequentialDataBlock(1, [True]*100),
            di=ModbusSequentialDataBlock(10001, [17]*100),
            hr=ModbusSequentialDataBlock(40001, [17]*100),
            ir=ModbusSequentialDataBlock(30000, [17]*100))

        slaves = {
            0x0a: self.slaveContext
        }

        context = ModbusServerContext(slaves=slaves, single=False)

        # ----------------------------------------------------------------------- #
        # initialize the server information
        # ----------------------------------------------------------------------- #
        # If you don't set this or any fields, they are defaulted to empty strings.
        # ----------------------------------------------------------------------- #
        identity = ModbusDeviceIdentification()
        identity.VendorName = 'Pymodbus'
        identity.ProductCode = 'PM'
        identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
        identity.ProductName = 'Pymodbus Server'
        identity.ModelName = 'Pymodbus Server'
        identity.MajorMinorRevision = '2.3.0'

        super(ModbusRTUSerialServer, self).__init__(context, ModbusRtuFramer, identity,
                                                    port=config["port"],
                                                    baudrate=config["baudrate"],
                                                    timeout=config["timeout"],
                                                    bytesize=serial.EIGHTBITS,
                                                    stopbits=serial.STOPBITS_ONE,
                                                    parity=config["parity"],
                                                    rtscts = False)
        for f in custom_functions:
            self.decoder.register(f)

    def getReadCoilsRequestCount(self):
        return self.slaveContext.store['c'].rCount

    def getWriteCoilsRequestCount(self):
        return self.slaveContext.store['c'].wCount

    def setCoils(self, address=0, bits=[]):
        self.slaveContext.store['c'].values = bits

    def getCoils(self, address=0):
        return self.slaveContext.store['c'].values

    def setInputRegister(self, address=0, registers=[]):
        self.slaveContext.store['i'].values = registers

    def getInputRegister(self, address=0):
        return self.slaveContext.store['i'].values

    def start(self):
        Thread(target=self.serve_forever).start()

class ModbusAction():
    def exec(self, line=""):
        args = line.split(' ')
        if args[0] == "set":
            return "OK"
        elif args[0] == "toggle":
            coils_bit = self.server.getCoils(0)
            self.server.setCoils(0, [not coils_bit[i] for i in range(len(coils_bit))])

            self.server.socket.setRTS(True)
            time.sleep(0.001)
            self.server.socket.setRTS(False)
            return "OK"
        else:
            return "ERROR"

    def __init__(self, rows, cols, start_row, start_col):
        self.server = ModbusRTUSerialServer()
        self.server.start()
        # curses.wrapper(self.__texttable_print)
        self.screen = curses.initscr()
        self.screen.clear()

        console_rows = 3
        rows = rows - console_rows
        self.cwin = curses.newwin(console_rows, cols,
                                  start_row, start_col)

        start_row = start_row + console_rows + 1
        self.twin = curses.newwin(rows, cols,
                                  start_row, start_col)
        # self.twin.nodelay(True)
        # curses.halfdelay(1)
        self.twin.timeout(500)
        # self.twin.keypad(1)
        # self.twin.border()

        self.cwin.addstr(1, 0, '> ')
        self.cwin.refresh()
        self.cwin.timeout(100)
        # self.cwin.keypad(1)
        # self.cwin.border()

        line_buffer = ""
        row_number = 0
        while(True):
            modbus_texttable = Texttable()
            modbus_texttable.set_precision(3)
            modbus_texttable.set_deco(Texttable.HEADER)
            modbus_texttable.set_cols_width([   config["col1_size"], config["col2_size"],
                                                config["col3_size"], config["col4_size"],
                                                config["col5_size"], config["col6_size"],
                                                config["col7_size"], config["col8_size"]])
            modbus_texttable.set_header_align([ "l", "l",
                                                "l", "l",
                                                "l", "l",
                                                "l", "l"])
            modbus_texttable.add_row([  "Count", "r:%d, w:%d" % (self.server.getReadCoilsRequestCount(), self.server.getWriteCoilsRequestCount()),
                                        "Count", "",
                                        "Count", "",
                                        "Count", ""])
            modbus_texttable.add_rows(modbus_make_table(coils_bits=self.server.getCoils(0),
                                                        input_r=self.server.getInputRegister(0)))
            
            self.twin.clrtoeol()
            try:
                self.twin.addstr(1, 0, modbus_texttable.draw())
            except Exception as e:
                self.twin.addstr(8, 0, "Exception: %s" % str(e))
            self.twin.refresh()

            key = self.cwin.getch()

            # This throws away any typeahead that has been typed by the user and has not yet been processed by the program
            # curses.flushinp()

            if key == -1:
                continue

            if key == ord('\n'):
                row_number = row_number + 1
                if(row_number >= rows):
                    row_number = 0
                self.cwin.addstr(0, 0, line_buffer)
                self.cwin.clrtoeol()
                self.cwin.addstr(1, 0, "")
                self.cwin.clrtoeol()
                
                result = self.exec(line_buffer)
                self.cwin.addstr(2, 0, result)
                self.cwin.clrtoeol()
                self.cwin.addstr(1, 0, "> ")
                self.cwin.clrtoeol()
                # self.cwin.insdelln()
                self.cwin.refresh()
                line_buffer = ""
            elif key == ord('r'):
                self.server.socket.setRTS(True)
                time.sleep(0.01)
                self.server.socket.setRTS(False)
            elif key == curses.KEY_UP:
                curses.beep()
                input_registers = self.server.getInputRegister(0)
                input_registers[0] = input_registers[0] + 1
                self.server.setInputRegister(0, input_registers)

            elif key == curses.KEY_DOWN:
                curses.beep()
                input_registers = self.server.getInputRegister(0)
                if(input_registers[0] > 0):
                    input_registers[0] = input_registers[0] - 1
                    self.server.setInputRegister(0, input_registers)

            elif key == curses.KEY_NPAGE:
                curses.beep()
            elif key == curses.KEY_PPAGE:
                curses.beep()
            elif key == curses.KEY_RIGHT:
                curses.beep()
            elif key == curses.KEY_LEFT:
                curses.beep()
            else:
                line_buffer += chr(key)




if __name__ == "__main__":
    # run_server()
    modbus_action = ModbusAction(20, 150, 0, 0)

