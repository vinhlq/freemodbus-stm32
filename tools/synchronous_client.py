#!/usr/bin/env python
"""
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

It should be noted that the client can also be used with
the guard construct that is available in python 2.5 and up::

    with ModbusClient('127.0.0.1') as client:
        result = client.read_coils(1,10)
        print result
"""
# --------------------------------------------------------------------------- #
# import the various server implementations
# --------------------------------------------------------------------------- #
# from pymodbus.client.sync import ModbusTcpClient as ModbusClient
# from pymodbus.client.sync import ModbusUdpClient as ModbusClient
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.exceptions import ConnectionException
from pymodbus.utilities import hexlify_packets, ModbusTransactionState
from pymodbus.utilities import pack_bitstring, unpack_bitstring
import struct
import sys
import time
from collections import OrderedDict
import pytz  # $ pip install pytz
import datetime
import threading
import configparser
# if sys.platform == 'win32':
#     import unicurses as curses
# else:
#     import curses
import curses
import serial
from texttable import Texttable


config={}
configparser = configparser.RawConfigParser()
configparser.read(r'config')
if len(sys.argv) >= 2:
    config["port"] = sys.argv[1]
else:
    config["port"]=configparser.get('serial', 'port');
if config["port"] == None:
    config["port"]='/dev/ttyUSB0'
config["baudrate"]=int(configparser.get('serial', 'baudrate'));
if config["baudrate"] == 0 or config["baudrate"] == None:
    config["baudrate"]=9600
config["timeout"]=float(configparser.get('serial', 'timeout'));
if config["timeout"] == None:
    config["timeout"]=10
parity=str(configparser.get('serial', 'parity'))
if parity == None:
    config["parity"]=serial.PARITY_NONE
elif parity.lower() == 'even':
    config["parity"]=serial.PARITY_EVEN
elif parity.lower() == 'odd':
    config["parity"]=serial.PARITY_ODD
elif parity.lower() == 'mark':
    config["parity"]=serial.PARITY_MARK
elif parity.lower() == 'space':
    config["parity"]=serial.PARITY_SPACE
else:
    config["parity"]=serial.PARITY_NONE
    
config["parity"]=serial.PARITY_NONE
    
config["col1_size"]=int(configparser.get('texttable', 'col1_size'))
if config["col1_size"] == None:
    config["col1_size"]=25
config["col2_size"]=int(configparser.get('texttable', 'col2_size'))
if config["col2_size"] == None:
    config["col2_size"]=19
config["col3_size"]=int(configparser.get('texttable', 'col3_size'))
if config["col3_size"] == None:
    config["col3_size"]=20
config["col4_size"]=int(configparser.get('texttable', 'col4_size'))
if config["col4_size"] == None:
    config["col4_size"]=8
config["col5_size"]=int(configparser.get('texttable', 'col5_size'))
if config["col5_size"] == None:
    config["col5_size"]=27
config["col6_size"]=int(configparser.get('texttable', 'col6_size'))
if config["col6_size"] == None:
    config["col6_size"]=27
config["col7_size"]=int(configparser.get('texttable', 'col7_size'))
if config["col7_size"] == None:
    config["col7_size"]=27
config["col8_size"]=int(configparser.get('texttable', 'col8_size'))
if config["col8_size"] == None:
    config["col8_size"]=27

COILS_OFFSET=int(configparser.get('modbus', 'COILS_OFFSET'))
DISCRETE_INPUT_OFFSET=int(configparser.get('modbus', 'DISCRETE_INPUT_OFFSET'))
HOLDING_OFFSET=int(configparser.get('modbus', 'HOLDING_OFFSET'))
INPUT_OFFSET=int(configparser.get('modbus', 'INPUT_OFFSET'))
UNIT=int(configparser.get('modbus', 'UNIT'))

# --------------------------------------------------------------------------- #
# configure the client logging
# --------------------------------------------------------------------------- #
import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
# log.setLevel(logging.DEBUG)
log.setLevel(logging.ERROR)

_logger = logging.getLogger(__name__)
class RtsModbusSerialClient(ModbusSerialClient):

    def _send(self, request):
        """ Sends data on the underlying socket

        If receive buffer still holds some data then flush it.

        Sleep if last send finished less than 3.5 character
        times ago.

        :param request: The encoded request to send
        :return: The number of bytes written
        """
        if not self.socket:
            raise ConnectionException(self.__str__())
        if request:
            self.socket.setRTS(False)
            try:
                waitingbytes = self._in_waiting()
                if waitingbytes:
                    result = self.socket.read(waitingbytes)
                    if _logger.isEnabledFor(logging.WARNING):
                        _logger.warning("Cleanup recv buffer before "
                                        "send: " + hexlify_packets(result))
            except NotImplementedError:
                pass

            size = self.socket.write(request)
            time.sleep(0.0022)
            self.socket.setRTS(True)
            return size
        return 0
    
    def _recv(self, size):
        """ Reads data from the underlying descriptor

        :param size: The number of bytes to read
        :return: The bytes read
        """
        if not self.socket:
            raise ConnectionException(self.__str__())
        if size is None:
            size = self._wait_for_data()
        self.socket.setRTS(True)
        result = self.socket.read(size)
        return result

TEMPERATURE_MAX=200

def bits_to_u32be(bits):
    u32be = 0
    for i in range(len(bits)):
        if(bits[i]):
            u32be |= 1<<i
    return u32be

def registers_to_string(registers):
    return struct.pack(">%dH" % (len(registers),), *registers).decode("ascii").split('\x00')[0]

def registers_to_hex(registers):
    if int(sys.version[0]) == 2:
        return struct.pack(">%dH" % (len(registers),), *registers).encode("hex")
    else:
        return struct.pack(">%dH" % (len(registers),), *registers).hex()

def registers_to_u32be(registers):
    (u32be,) = struct.unpack('>I', struct.pack('>HH', registers[0], registers[1]))
    return u32be

def registers_to_i32be(registers):
    (u32be,) = struct.unpack('>i', struct.pack('>HH', registers[0], registers[1]))
    return u32be

def registers_to_u16be(registers):
    (u16be,) = struct.unpack('>h', struct.pack('>H', registers[0]))
    return u16be

def registers_to_float32(registers):
    (f32be,) = struct.unpack('>f', struct.pack('>HH', registers[0], registers[1]))
    return f32be

def registers_to_temperature(registers):
    return registers[0]/100

def registers_to_percentage(registers):
    return registers[0]/100
    

def registers_to_time(registers):
    (u32be,) = struct.unpack('>i', struct.pack('>HH', registers[0], registers[1]))
    return datetime.datetime.utcfromtimestamp(u32be)
        
def registers_get_next_system_mode(registers, step):
    if (registers[0]+step) >= 0 and (registers[0]+step) < 5:
        return [registers[0]+step]
    else:
        return [registers[0]]
    
def registers_float32_change(registers,step):
    (f32be,) = struct.unpack('>f', struct.pack('>HH', registers[0], registers[1]))
    return struct.unpack('>HH', struct.pack('>f', f32be+(step*1)))

def registers_u16be_change(registers,step):
    if (registers[0] + step) >= 0 and (registers[0] + step) < 65535:
        return [registers[0] + step]
    else:
        return [registers[0]]

def registers_setbit(registers,bit):
    if(bit >= 0 and bit <= 15):
        return [registers[0]|(1<<bit)]
    elif(bit < 0 and bit >= -15):
        return [registers[0] & (~(1<<bit))]
    
def bits_to_string(s):
    return str(s) if s<=1 else bits_to_string(s>>1) + str(s&1)

def bits_to_string8(u8):
    return '0b' + bin(u8%256).lstrip('-0b').zfill(8)

def bits_to_string16(u16):
    return bin(int(u16/256)).lstrip('-0b').zfill(8) + " " + bin(u16%256).lstrip('-0b').zfill(8)

def bit_isset(val,bit):
    if val & (1<<bit):
        return "1"
    else:
        return "-"


def modbus_make_table(coils_r, discrete_input_r, input_r, holding_r):
    coils_d = OrderedDict()
    discrete_input_d = OrderedDict()
    holding_d = OrderedDict()
    input_d = OrderedDict()

    if(coils_r):
        coils_d["%d(%d)" % (COILS_OFFSET, len(coils_r.bits))] =               (COILS_OFFSET, "", "%s" % (bits_to_string8(bits_to_u32be(coils_r.bits))), None)
    if(input_r):
        input_d["%d(%d)" % (INPUT_OFFSET, len(input_r.registers))] =        (INPUT_OFFSET, "", registers_to_u16be(input_r.registers[0:]), None)
    if(holding_r):
        holding_d["%d(%d)" % (HOLDING_OFFSET, len(holding_r.registers))] =  (HOLDING_OFFSET, "", registers_to_u16be(holding_r.registers[0:]), None)
    
    
    table_data = [[ "coils", "value",
                    "discrete_input", "value",
                    "input", "value",
                    "holding", "value"]]
    
    coils_items=list(coils_d.items())
    discrete_input_items=list(discrete_input_d.items())
    input_items=list(input_d.items())
    holding_items=list(holding_d.items())
    table_len = len(input_items)
    if table_len < len(coils_items):
        table_len = len(coils_items)
    if table_len < len(discrete_input_items):
        table_len = len(discrete_input_items)
    if table_len < len(input_items):
        table_len = len(input_items)
    if table_len < len(holding_items):
        table_len = len(holding_items)
        
    for i in range(table_len):
        table_data.append([])
        st = "----------------------------------------"
        
        if i < len(coils_items):
            (key,value)=coils_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col1_size"], st))
                table_data[i+1].append("%.*s" % (config["col2_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')

        if i < len(discrete_input_items):
            (key,value)=discrete_input_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col3_size"], st))
                table_data[i+1].append("%.*s" % (config["col4_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')
            
        if i < len(input_items):
            (key,value)=input_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col5_size"], st))
                table_data[i+1].append("%.*s" % (config["col6_size"], st))
            else:
                (_,value_string,value,_)=value
                if value_string == "":
                    value_string = str(value)
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')


        if i < len(holding_items):
            (key,value)=holding_items[i]
            if value == None:
                table_data[i+1].append("%.*s" % (config["col7_size"], st))
                table_data[i+1].append("%.*s" % (config["col8_size"], st))
            else:
                (address,value_string,value,_)=value
                if value_string == "":
                    value_string = value
                table_data[i+1].append(key)
                table_data[i+1].append(value_string)
        else:
            table_data[i+1].append('')
            table_data[i+1].append('')

    return table_data
    
class ModbusAction():
    def exec(self, line=""):
        args = line.split(' ')
        if args[0] == "set":
            return "OK"
        elif args[0] == "on":
            coils_w = self.client.write_coils(0, [True]*8, unit=UNIT)
            self.last_write_status = "register: coils, address: %d, size: %d, value: %d" % (1, 8, 1)
            if(coils_w.isError()):
                self.last_write_status = "%s, status: error" % (self.last_write_status)
            else:
                self.last_write_status = "%s, status: ok" % (self.last_write_status)
            return "OK"
        elif args[0] == "off":
            coils_w = self.client.write_coils(0, [False]*8, unit=UNIT)
            self.last_write_status = "register: coils, address: %d, size: %d, value: %d" % (1, 8, 1)
            if(coils_w.isError()):
                self.last_write_status = "%s, status: error" % (self.last_write_status)
            else:
                self.last_write_status = "%s, status: ok" % (self.last_write_status)
            return "OK"
        elif args[0] == "toggle":
            if len(self.write_coils_bits) == 0:
                    self.write_coils_bits = [True]*8
            for i in range(len(self.write_coils_bits)):
                self.write_coils_bits[i] = not self.write_coils_bits[i]
            coils_w = self.client.write_coils(0, self.write_coils_bits, unit=UNIT)
            self.last_write_status = "register: coils, address: %d, size: %d, value: " % (1, 8)
            self.last_write_status += repr(self.write_coils_bits)
            if(coils_w.isError()):
                self.last_write_status = "%s: ERROR" % (self.last_write_status)
                return "ERROR"
            else:
                self.last_write_status = "%s: OK" % (self.last_write_status)
                return "OK"
        elif args[0] == "blink":
            holding_w = self.client.write_register(HOLDING_OFFSET-1, 3, unit=UNIT)
            self.last_write_status = "register: holding, address: %d, size: %d, value: " % (HOLDING_OFFSET, 1)
            if(holding_w.isError()):
                self.last_write_status = "%s: ERROR" % (self.last_write_status)
                return "ERROR"
            else:
                self.last_write_status = "%s: OK" % (self.last_write_status)
                return "OK"

        # command is not found
        return "ERROR"

    def __init__(self, rows, cols, start_row, start_col):
        self.screen = curses.initscr()
        self.screen.clear()
        # window.nodelay(True)
        curses.halfdelay(1)
        curses.noecho()
        # curses.nocbreak()
        # window.keypad(True)
        # curses.noecho()

        self.client = ModbusSerialClient(method='rtu',
                                    port=config["port"],
                                    # timeout=config["timeout"],
                                    timeout=3,
                                    baudrate=config["baudrate"],
                                    bytesize=serial.EIGHTBITS,
                                    stopbits=serial.STOPBITS_ONE,
                                    parity=config["parity"],
                                    rtscts = False)
        self.client.connect()

        console_rows = 3
        rows = rows - console_rows
        self.cwin = curses.newwin(console_rows, cols,
                                  start_row, start_col)

        start_row = start_row + console_rows + 1
        self.twin = curses.newwin(rows, cols,
                                  start_row, start_col)
        # self.twin.nodelay(True)
        # curses.halfdelay(1)
        self.twin.timeout(500)
        # self.twin.keypad(1)
        # self.twin.border()

        self.cwin.addstr(1, 0, '> ')
        self.cwin.refresh()
        self.cwin.timeout(10)
        # self.cwin.keypad(1)
        # self.cwin.border()

        line_buffer = ""
        row_number = 0
    
        self.last_write_status = ""
        self.write_coils_bits = []
        curses.beep()
        while(True):
            modbus_texttable = Texttable()
            modbus_texttable.set_precision(3)
            modbus_texttable.set_deco(Texttable.HEADER)
            modbus_texttable.set_cols_width([   config["col1_size"], config["col2_size"],
                                                config["col3_size"], config["col4_size"],
                                                config["col5_size"], config["col6_size"],
                                                config["col7_size"], config["col8_size"]])
            modbus_texttable.set_header_align([ "l", "l",
                                                "l", "l",
                                                "l", "l",
                                                "l", "l"])
            (coils_r, discrete_input_r, input_r, holding_r) = self.get_modbus_data()
            modbus_texttable.add_rows(modbus_make_table(coils_r, discrete_input_r, input_r, holding_r))
    #         modbus_texttable.draw()
            
            try:
                self.twin.addstr(1, 0, "Modbus last write: %s" % (self.last_write_status))
                self.twin.clrtoeol()
                self.twin.addstr(3, 0, modbus_texttable.draw())
                self.twin.clrtoeol()
            except Exception as e:
                self.twin.addstr(8, 0, "Exception: %s" % str(e))
                self.twin.clrtoeol()
            self.twin.refresh()

            key = self.cwin.getch()
            # curses.flushinp()

            if key == -1:
                continue
            
            if key == 127:
                line_buffer = line_buffer[:-1]
            elif key == ord('\n'):
                row_number = row_number + 1
                if(row_number >= rows):
                    row_number = 0
                self.cwin.addstr(0, 0, line_buffer)
                self.cwin.clrtoeol()
                self.cwin.addstr(1, 0, "")
                self.cwin.clrtoeol()
                
                result = self.exec(line_buffer)
                self.cwin.addstr(2, 0, result)
                self.cwin.clrtoeol()
                self.cwin.addstr(1, 0, "> ")
                self.cwin.clrtoeol()
                # self.cwin.insdelln()
                self.cwin.refresh()
                line_buffer = ""
            elif key == curses.KEY_UP:
                curses.beep()
            elif key == curses.KEY_DOWN:
                curses.beep()
            elif key == curses.KEY_NPAGE:
                curses.beep()
            elif key == curses.KEY_PPAGE:
                curses.beep()
            elif key == curses.KEY_RIGHT:
                curses.beep()
            elif key == curses.KEY_LEFT:
                curses.beep()
            else:
                line_buffer += chr(key)

            self.cwin.addstr(1, 0, "> %s" % line_buffer)
            self.cwin.clrtoeol()
            self.cwin.refresh()

    def get_modbus_data(self):
        coils_r = None
        discrete_input_r = None
        input_r = None
        holding_r = None

        coils_r = self.client.read_coils(COILS_OFFSET, 8, unit=UNIT)
        if(coils_r.isError()):
            coils_r = None

        discrete_input_r = self.client.read_discrete_inputs(DISCRETE_INPUT_OFFSET, 1, unit=UNIT)
        if(discrete_input_r.isError()):
            discrete_input_r = None

        input_r = self.client.read_input_registers(INPUT_OFFSET, 1, unit=UNIT)
        if(input_r.isError()):
            input_r = None

        # holding_r = self.client.read_holding_registers(HOLDING_OFFSET, 1, unit=UNIT)
        # if(holding_r.isError()):
        #     holding_r = None

        return (coils_r, discrete_input_r, input_r, holding_r)


    


if __name__ == "__main__":
    modbus_action = ModbusAction(20, 150, 0, 0)

    
